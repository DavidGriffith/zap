CFLAGS = -g

ZAPOBJS = zap.o zasm.o zdir.o zop.o zlabel.o zeval.o zfixup.o zerror.o \
	zlex.o zsymtab.o zdata.o zobj.o zstring.o zsymout.o ztable.o \
	zsegment.o alloc.o nmemory.o resfile.o zapres.o comptime.o
ZAPSRCS = zap.c zasm.c zdir.c zop.c zlabel.c zeval.c zfixup.c zerror.c \
	zlex.c zsymtab.c zdata.c zobj.c zstring.c zsymout.c ztable.c \
	zsegment.c alloc.c nmemory.c resfile.c zapres.c comptime.c

ZGLUEOBJS = zglue.o comptime.o
ZGLUESRCS = zglue.c comptime.c

ZSPLITOBJS = zsplit.o zspix.o zsres.o resfile.o
ZSPLITSRCS = zsplit.c zspix.c zsres.c resfile.c

ALL =	zap zsplit zglue zsymtest

all:	$(ALL)

zap:	$(ZAPOBJS)
	$(CC) -o zap $(ZAPOBJS)

zsymtest:	zsymtest.o
	$(CC) -o zsymtest zsymtest.o

zglue:	$(ZGLUEOBJS) splitdefs.h
	$(CC) $(CFLAGS) -o zglue $(ZGLUEOBJS)

zsplit:	$(ZSPLITOBJS)
	$(CC) -o zsplit $(ZSPLITOBJS)

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<


clean:
	rm -f $(ALL)
	rm -f *.o
